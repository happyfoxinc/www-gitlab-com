---
layout: handbook-page-toc
title: "People Group READMEs"
description: "The READMEs for the People Group team at GitLab can be found on this page."
---

## People Group READMEs

- [Ashley Jones's README](/handbook/people-group/readmes/asjones/)
- [Trevor Knudsen's README](/handbook/people-group/readmes/tknudsen)

